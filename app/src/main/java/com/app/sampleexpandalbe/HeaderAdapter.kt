package com.app.sampleexpandalbe

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


class HeaderAdapter(private val list: List<Response>) : RecyclerView.Adapter<RecyclerView.ViewHolder>(),StickHeaderItemDecoration.StickyHeaderInterface {


    override fun getHeaderPositionForItem(itemPosition: Int): Int {
        var itemPositionData = itemPosition
        var headerPosition = 0
        do {
            if (isHeader(itemPosition)) {
                headerPosition = itemPosition
                break
            }
            itemPositionData -= 1
        } while (itemPositionData >= 0)
        return headerPosition
    }

    override fun getHeaderLayout(headerPosition: Int): Int {
        return if (list[headerPosition].viewType == 1)
            R.layout.header_1
        else {
            R.layout.header_2
        }
    }

    override fun bindHeaderData(header: View?, headerPosition: Int) {

    }

    override fun isHeader(itemPosition: Int): Boolean {
        return list[itemPosition].viewType == 1 || list[itemPosition].viewType == 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            1 -> HeaderViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.header_1, parent, false)
            )
            2 -> Header2ViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.header_2, parent, false)
            )
            else -> ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder -> {
                holder.bindData(position)
            }
            is HeaderViewHolder -> {
                holder.bindData(position)
            }
            is Header2ViewHolder -> {
                holder.bindData(position)
            }
        }
    }

    override fun getItemCount() = list.size

    override fun getItemViewType(position: Int): Int {
        return list[position].viewType
    }

    internal class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(pos:Int){

        }
    }

    internal class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(pos:Int){

        }
    }
    internal class Header2ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(pos:Int){

        }
    }

}