package com.app.sampleexpandalbe

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var data: ArrayList<Response> = getDataList()

    private val adapter by lazy {
        HeaderAdapter(data)
    }

    private fun getDataList() : ArrayList<Response>{
        val mData = ArrayList<Response>()
        mData.add(Response(1))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(2))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(1))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(2))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))
        mData.add(Response(0))

        return mData
    }

  /*  private fun getDataList(): ArrayList<Response> {
        val dataList: ArrayList<DataList> = emptyList<DataList>() as ArrayList<DataList>
        dataList.add(DataList("Desc 1"))
        dataList.add(DataList("Desc 2"))
        dataList.add(DataList("Desc 3"))
        dataList.add(DataList("Desc 4"))
        val dataList1: ArrayList<DataList> = emptyList<DataList>() as ArrayList<DataList>
        dataList1.add(DataList("Desc 1"))
        dataList1.add(DataList("Desc 2"))
        dataList1.add(DataList("Desc 3"))
        dataList1.add(DataList("Desc 4"))
        data.add(Response(dataList = dataList1, header = "Header 2"))
        val dataList3: ArrayList<DataList> = emptyList<DataList>() as ArrayList<DataList>
        dataList3.add(DataList("Desc 1"))
        dataList3.add(DataList("Desc 2"))
        dataList3.add(DataList("Desc 3"))
        dataList3.add(DataList("Desc 4"))
        data.add(Response(dataList = dataList3, header = "Header 2"))

        return data
    }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv_sticky.adapter= adapter
        rv_sticky.addItemDecoration(StickHeaderItemDecoration(adapter))
    }


}