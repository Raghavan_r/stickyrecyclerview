package com.app.sampleexpandalbe


data class Response(
   val viewType:Int
)
data class DataList(
    val description: String?
)